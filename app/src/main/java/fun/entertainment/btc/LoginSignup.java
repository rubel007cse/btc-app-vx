package fun.entertainment.btc;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;


import fun.entertainment.btc.fragments.Login;
import fun.entertainment.btc.fragments.Signup;

public class LoginSignup extends AppCompatActivity {


    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sigininlogin);

        Fragment fragment = new Login();
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragment_place, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();


        final Button goToSignup  = findViewById(R.id.signupbutton);
        final Button goToLogin = findViewById(R.id.loginbutton);

        goToSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                goToSignup.setBackgroundColor(Color.WHITE);
                goToLogin.setBackgroundColor(Color.rgb(245,245,245));
                Fragment fragment = new Signup();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();

            }
        });

        goToLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Fragment fragment = new Login();
                FragmentManager fragmentManager = getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragment_place, fragment);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
                goToLogin.setBackgroundColor(Color.WHITE);
                goToSignup.setBackgroundColor(Color.rgb(245,245,245));


            }
        });


    }

    @Override
    public void onBackPressed() {
        finish();
    }
}