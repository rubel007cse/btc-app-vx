package fun.entertainment.btc.fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import fun.entertainment.btc.ConstantData;
import fun.entertainment.btc.ModelRecycler;
import fun.entertainment.btc.R;
import fun.entertainment.btc.RetroInterface;
import fun.entertainment.btc.RetrofitAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class SecondFragment extends Fragment {

    RetrofitAdapter retrofitAdapter;
    RecyclerView recyclerView;
    Dialog myDialog;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){

        final View rootView = inflater.inflate(R.layout.secondlayout, viewGroup, false);
        myDialog = new Dialog(getActivity());


        recyclerView = rootView.findViewById(R.id.recycler_f2);

        fetchJSON();


        recyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(getActivity(), recyclerView ,new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // do whatever

                        //Toast.makeText(getActivity(), "Short", Toast.LENGTH_SHORT).show();
                        // showPopup(view);
                        try {

                            JSONArray ns_data = ConstantData.dataobj_store_f2;
                            JSONObject dataobj = ns_data.getJSONObject(position);

                            String[] data = { dataobj.getString("image"),
                                    dataobj.getString("headline"),
                                    dataobj.getString("description"),
                                    dataobj.getString("datetime"),
                            };

                           // Intent nextPage = new Intent(getActivity(), NewsDetails.class);
                           // nextPage.putExtra("nd_extras",data);
                           // startActivity(nextPage);

                        } catch (NullPointerException e){
                            Log.e("addOnItemTouchListener", ""+e);
                        } catch (JSONException e){
                            Log.e("HomeFragmentJson", ""+e);
                        }



                    }

                    @Override
                    public void onLongItemClick(View view, int position) {
                        // do whatever
                        Toast.makeText(getActivity(), "Long", Toast.LENGTH_SHORT).show();
                    }
                })
        );




        return rootView;
    }



    private void fetchJSON(){



        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMessage("Please wait....");
        progressDoalog.setIndeterminate(true);
        progressDoalog.setCanceledOnTouchOutside(false);
        // show it
        progressDoalog.show();



        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(RetroInterface.JSONURL)
                .addConverterFactory(ScalarsConverterFactory.create())
                .build();

        RetroInterface api = retrofit.create(RetroInterface.class);

        Call<String> call = api.getStringTabOne();

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {


                // close it after response
                progressDoalog.dismiss();

                Log.i("Responsestring", response.body().toString());
                //Toast.makeText()
                if (response.isSuccessful()) {
                    if (response.body() != null) {
                        Log.i("onSuccess", response.body().toString());

                        String jsonresponse = response.body().toString();
                        writeRecycler(jsonresponse);

                    } else {
                        Log.i("onEmptyResponse", "Returned empty response");//Toast.makeText(getContext(),"Nothing returned",Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

                // close it after response
                progressDoalog.dismiss();

            }
        });
    }

    private void writeRecycler(String response){

        try {
            //getting the whole json object from the response
            JSONObject obj = new JSONObject(response);
            if(obj.optString("status").equals("true")){

                ArrayList<ModelRecycler> modelRecyclerArrayList = new ArrayList<>();
                JSONArray dataArray  = obj.getJSONArray("data");
                ConstantData.dataobj_store_f2 = dataArray;

                for (int i = 0; i < dataArray.length(); i++) {

                    ModelRecycler modelRecycler = new ModelRecycler();
                    JSONObject dataobj = dataArray.getJSONObject(i);

                    modelRecycler.setDescription(dataobj.getString("description"));
                    modelRecycler.setImgURL(dataobj.getString("imgurl"));
                    modelRecycler.setUserName(dataobj.getString("uname"));
                    modelRecycler.setUserImg(dataobj.getString("photo"));
                    modelRecycler.setTime(dataobj.getString("created"));

                    modelRecyclerArrayList.add(modelRecycler);


                }

                retrofitAdapter = new RetrofitAdapter(getActivity(), modelRecyclerArrayList);
                recyclerView.setAdapter(retrofitAdapter);
                recyclerView.setLayoutManager(
                        new LinearLayoutManager(getActivity(),
                                RecyclerView.VERTICAL,
                                false)
                );

//                recyclerView.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//
//                        Toast.makeText(getActivity(), "Clicekd", Toast.LENGTH_SHORT).show();
//                    }
//                });
//


            }else {
                Toast.makeText(getActivity(), obj.optString("message")+"", Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }



}



