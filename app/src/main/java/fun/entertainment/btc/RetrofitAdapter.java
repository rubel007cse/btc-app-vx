package fun.entertainment.btc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class RetrofitAdapter extends RecyclerView.Adapter<RetrofitAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<ModelRecycler> dataModelArrayList;

    public RetrofitAdapter(Context ctx, ArrayList<ModelRecycler> dataModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.dataModelArrayList = dataModelArrayList;
    }

    @Override
    public RetrofitAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.retro_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RetrofitAdapter.MyViewHolder holder, int position) {

        Picasso.get().load(dataModelArrayList.get(position).getImgURL()).into(holder.iv);
        Picasso.get().load(dataModelArrayList.get(position).getUserImg()).into(holder.userImg);

        holder.description.setText(dataModelArrayList.get(position).getDescription());
        holder.dtime.setText(dataModelArrayList.get(position).getTime());

        holder.userName.setText(dataModelArrayList.get(position).getUserName());


    }

    @Override
    public int getItemCount() {
        return dataModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView description, dtime, userName;
        ImageView iv, userImg;

        MyViewHolder(View itemView) {
            super(itemView);

            description = itemView.findViewById(R.id.description);
            dtime = itemView.findViewById(R.id.time);
            userName = itemView.findViewById(R.id.userName);
            iv =  itemView.findViewById(R.id.iv);
            userImg =  itemView.findViewById(R.id.userImg);
        }

    }
}