package fun.entertainment.btc;

public class ModelRecycler {

    private String headline, description, imgURL, time, userName, userImg;

    String getDescription() {
        return description;
    }

    String getHeadline() {
        return headline;
    }

    String getImgURL() {
        return imgURL;
    }

    String getTime() {
        return time;
    }

    String getUserImg() {
        return userImg;
    }

    String getUserName() {
        return userName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public void setImgURL(String imgURL) {
        this.imgURL = imgURL;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
