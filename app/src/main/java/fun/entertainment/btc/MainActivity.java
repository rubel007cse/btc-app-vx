package fun.entertainment.btc;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.view.MenuItem;

import fun.entertainment.btc.fragments.HomeFragment;
import fun.entertainment.btc.fragments.SecondFragment;
import fun.entertainment.btc.fragments.LovedMemes;
import fun.entertainment.btc.profile.Profile;
import fun.entertainment.btc.uploadmeme.UploadMeme;

public class MainActivity extends AppCompatActivity {

    final Fragment home_fragment = new HomeFragment();
    final Fragment second_fragment = new SecondFragment();
    final Fragment third_fragment = new LovedMemes();
    final Fragment fourth_fragment = new UploadMeme();
    final Fragment fifth_fragment = new Profile();
    final FragmentManager fm = getSupportFragmentManager();


    Fragment active = home_fragment;
    Toolbar toolbar;
    // login check
    boolean isLoggedIn = true;
    BottomNavigationView navView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_home_black_24dp);

        fm.beginTransaction().add(R.id.main_container, fifth_fragment, "5").hide(fifth_fragment).commit();
        fm.beginTransaction().add(R.id.main_container, fourth_fragment, "4").hide(fourth_fragment).commit();
        fm.beginTransaction().add(R.id.main_container, third_fragment, "3").hide(third_fragment).commit();
        fm.beginTransaction().add(R.id.main_container, second_fragment, "2").hide(second_fragment).commit();
        fm.beginTransaction().add(R.id.main_container,home_fragment, "1").commit();




        if(!haveNetworkConnection()){


            new AlertDialog.Builder(this)
                    .setTitle("Oops! No Internet!")
                    .setMessage("Please connect with Wifi or Mobile Internet!")

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            // Continue with delete operation
                            finish();
                        }
                    })
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setCancelable(false)
                    .show();

        } else {

            navView = findViewById(R.id.navigation);
            navView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        }


    }



    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    fm.beginTransaction().hide(active).show(home_fragment).commit();
                    active = home_fragment;
                    toolbar.setTitle("BTC Feed");
                    return true;
                case R.id.navigation_dashboard:
                    fm.beginTransaction().hide(active).show(second_fragment).commit();
                    active = second_fragment;
                    toolbar.setTitle("Hot Trolls");
                    return true;
                case R.id.navigation_notiafications:
                    fm.beginTransaction().hide(active).show(third_fragment).commit();
                    active = third_fragment;
                    toolbar.setTitle("Loved Trolls");
                    return true;
                case R.id.navigation_uploads:
                    fm.beginTransaction().hide(active).show(fourth_fragment).commit();
                    active = fourth_fragment;
                    toolbar.setTitle("Add my own meme");
                    return true;
                case R.id.navigation_person:

                    if(!isLoggedIn){
                        startActivity(new Intent(getApplicationContext(), LoginSignup.class));
                    } else {
                        fm.beginTransaction().hide(active).show(fifth_fragment).commit();
                        active = fifth_fragment;
                        toolbar.setTitle("My Profile");
                        return true;
                    }

            }
            return false;
        }
    };



    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }


}
