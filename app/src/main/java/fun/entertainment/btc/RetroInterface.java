package fun.entertainment.btc;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface RetroInterface {

    String JSONURL = "http://btcmedia.xyz/btc/api/";

    @GET("btc_memes.php?showintab=0")
    Call<String> getString();


    @GET("btc_memes.php?showintab=1")
    Call<String> getStringTabOne();

    @GET("btc_memes.php?showintab=2")
    Call<String> getStringTabTwo();

    // Upload meme
    @Multipart
    @POST("upload.php")
    Call<ResponseBody> uploadImage(@Part MultipartBody.Part file,
                                   @Part("file") RequestBody name,
                                    @Part("description") RequestBody desc,
                                    @Part("puser") int user);


}
