package fun.entertainment.btc.uploadmeme;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;

import java.io.File;
import java.io.IOException;

import fun.entertainment.btc.MainActivity;
import fun.entertainment.btc.R;
import fun.entertainment.btc.RetroInterface;
import fun.entertainment.btc.profile.Profile;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static android.app.Activity.RESULT_OK;

public class UploadMeme extends Fragment {

    ImageView imageView;
    EditText captions;
    static final int GALLERY_REQUEST_CODE = 20;
    String realPath = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState){

        final View rootView = inflater.inflate(R.layout.upload_memes, viewGroup, false);



        BottomNavigationView navView = rootView.findViewById(R.id.navigation);

        imageView = rootView.findViewById(R.id.selectimage);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                        1);

                pickFromGallery();

            }
        });

        captions = rootView.findViewById(R.id.captions);


        Button uloadImg = rootView.findViewById(R.id.uploadimage);
        uloadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(realPath == null){
                    Toast.makeText(getActivity(), "Click on image to add meme from your phone..",
                            Toast.LENGTH_LONG).show();

                } else if (captions.getText().toString().length() < 5){
                    Toast.makeText(getActivity(), "Please add a bigger caption!",
                            Toast.LENGTH_LONG).show();
                }
                else {

                    uploadToServer(realPath, captions.getText().toString(), 2);
                    Log.d("uploaddesc",captions.getText().toString());
                }

            }
        });



        return rootView;
    }





    private void uploadToServer(String filePath, String desc, int user) {


        // Set up progress before call
        final ProgressDialog progressDoalog;
        progressDoalog = new ProgressDialog(getActivity());
        progressDoalog.setMessage("Please wait....");
        progressDoalog.setIndeterminate(true);
        progressDoalog.setCanceledOnTouchOutside(false);
        // show it
        progressDoalog.show();

        Retrofit retrofit = NetworkClient.getRetrofitClient(getActivity());

        RetroInterface uploadAPIs = retrofit.create(RetroInterface.class);

        //Create a file object using file path
        File file = new File(filePath);

        // Create a request body with file and image media type
        RequestBody fileReqBody = RequestBody.create(MediaType.parse("image/*"), file);

        // Create MultipartBody.Part using file request-body,file name and part name
        MultipartBody.Part part = MultipartBody.Part.createFormData("file", file.getName(), fileReqBody);

        //Create request body with text description and text media type
        RequestBody description = RequestBody.create(MediaType.parse("text/plain"), "image-type");

        RequestBody caption = RequestBody.create(MediaType.parse("text/plain"),desc);

        //
        Call call = uploadAPIs.uploadImage(part, description,caption , user);
       // Log.d("dataforImg",part.toString()+"--"+description.toString());

        call.enqueue(new Callback() {
            @Override
            public void onResponse(@NonNull Call call, @NonNull Response response) {

                // close it after response
                progressDoalog.dismiss();


                if(response.isSuccessful()){

                    refreshFragment();

                    Toast.makeText(getActivity(), "Successfully meme uploaded!",
                            Toast.LENGTH_LONG).show();



                } else {
                    Toast.makeText(getActivity(), response.body().toString(),
                            Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call call, Throwable t) {

                // close it after response
                progressDoalog.dismiss();

                Toast.makeText(getActivity(), "Something went wrong! Try in few minutes..",
                        Toast.LENGTH_LONG).show();


            }
        });



    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        try {
            super.onActivityResult(requestCode, resultCode, data);

            if (requestCode == GALLERY_REQUEST_CODE  && resultCode  == RESULT_OK) {



                Uri uri = data.getData();


                 realPath = getPath(getActivity(), data.getData());
                //realPath = RealPathUtil.getRealPathFromURI_API19(this, data.getData());

                Log.i("ImageUploadPath", "onActivityResult: file path : " + realPath);
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), uri);
                    // Log.d(TAG, String.valueOf(bitmap));

                    imageView.setImageBitmap(bitmap);
                    imageView.setVisibility(View.VISIBLE);

                } catch (IOException e) {
                    e.printStackTrace();
                }



            }
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.toString(),
                    Toast.LENGTH_SHORT).show();
        }

    }


    private void pickFromGallery(){
        //Create an Intent with action as ACTION_PICK
        Intent intent=new Intent(Intent.ACTION_PICK);
        // Sets the type as image/*. This ensures only components of type image are selected
        intent.setType("image/*");
        //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
        String[] mimeTypes = {"image/jpeg", "image/png"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
        // Launching the Intent
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }



    private static String getPath(Context context, Uri uri ) {
        String result = null;
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver( ).query( uri, proj, null, null, null );
        if(cursor != null){
            if ( cursor.moveToFirst( ) ) {
                int column_index = cursor.getColumnIndexOrThrow( proj[0] );
                result = cursor.getString( column_index );
            }
            cursor.close( );
        }
        if(result == null) {
            result = "Not found";
        }
        return result;
    }


    private void refreshFragment(){


            BottomNavigationView mBottomNavigationView = getActivity().findViewById(R.id.navigation);
            mBottomNavigationView.setSelectedItemId(R.id.navigation_person);

            FragmentManager fm = getActivity().getSupportFragmentManager();
            fm.beginTransaction().remove(new UploadMeme()).show(new Profile()).commit();


    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],@NonNull int[] grantResults) {
        if (requestCode == 1) {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }

            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

}


